// 1. Program that gets the area and perimeter of a quadrilateral
public class Quadrilateral {
    // Attributes
    private int side01, side02;

    // Constructors
    public Quadrilateral(int side01, int side02){
        setSide01(side01);
        setSide02(side02);
    }

    public Quadrilateral(int side01){
        this.side01 = this.side02 = side01;
    }

    // Getters
    public int getSide01() {
        return side01;
    }

    public int getSide02() {
        return side02;
    }

    public int getPerimeter() {
        return 2 * (this.side01 + this.side02);
    }

    public int getArea() {
        return this.side01 * this.side02;
    }

    // Setters

    public void setSide01(int side01) {
        this.side01 = side01;
    }


    public void setSide02(int side02) {
        this.side02 = side02;
    }
}
