//4. Program that given a number of athletes with their attributes, shows
// all the attributes of the winner (based on time).
import java.util.Scanner;

public class Athlete {
    // Attributes
    private String name;
    private int id;
    private float time;

    // Constructor
    public Athlete(int id, String name, float time){
        setName(name);
        setId(id);
        setTime(time);
    }

    // Getters and Setters

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getTime() {
        return time;
    }

    public void setTime(float time) {
        this.time = time;
    }

    // Methods
    public void showAttributes(){
        System.out.printf("ID: %d \nName: %s \nTime: %.2f seg\n",
                getId(), getName(), getTime());
    }

    // Main
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String name;
        int position = 0, elements, id;
        float winner = 0, time;

        System.out.print("How many athletes would you like to enter?: ");
        elements = input.nextInt();

        Athlete[] athletes = new Athlete[elements];

        for (int i = 0; i < elements; i++) {
            System.out.print("Enter the athlete's id: ");
            id = input.nextInt();
            System.out.print("Enter the athlete's name: ");
            name = input.next();
            System.out.print("Enter the athlete's time: ");
            time = input.nextFloat();

            athletes[i] = new Athlete(id, name, time);
        }

        winner = athletes[0].time;

        for (int i = 0; i < athletes.length; i++) {
            if (athletes[i].time < winner){
                winner = athletes[i].time;
                position = i;
            }
        }

        System.out.println("--- WINNER ---");
        athletes[position].showAttributes();

    }
}
