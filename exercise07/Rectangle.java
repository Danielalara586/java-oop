public class Rectangle extends Polygon {
    float side01, side02;
    public Rectangle(float side01, float side02) {
        super(2, "rectangle");
        this.side01 = side01;
        this.side02 = side02;
    }

    @Override
    public float area() {
        return side01 * side02;
    }

    @Override
    public String showData() {
        return super.showData() + "Side 1: " + side01 + "cm. \n" +
                "Side 2: " + side02 + "cm. \n";
    }
}
