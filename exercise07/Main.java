import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int numPolygons;
        float side01, side02, side03;
        char option;

        System.out.print("How many polygons would you like to enter?: ");
        numPolygons = input.nextInt();

        Polygon[] polygons = new Polygon[numPolygons];

        for (int i = 0; i < numPolygons; i++) {
            System.out.print("What polygon would you like to enter? " +
                    "[R]ectangle, [T]riangle: ");
            option = input.next().charAt(0);


            switch (Character.toUpperCase(option)){
                case 'R':
                    System.out.print("Please enter the side 1: ");
                    side01 = input.nextFloat();
                    System.out.print("Please enter the side 2: ");
                    side02 = input.nextFloat();
                    polygons[i] = new Rectangle(side01, side02);
                    break;
                case 'T':
                    System.out.print("Please enter the side 1: ");
                    side01 = input.nextFloat();
                    System.out.print("Please enter the side 2: ");
                    side02 = input.nextFloat();
                    System.out.print("Please enter the side 3: ");
                    side03 = input.nextFloat();
                    polygons[i] = new Triangle(side01, side02, side03);
                    break;
            }
        }

        System.out.println("\n<---- SHOW DATA ---->");
        for (Polygon polygon : polygons){
            System.out.println(polygon.showData()+"\n");
        }
    }
}
