//7. Program that fills up a polygon array given n elements
// It can be filled with rectangles and triangles.

public abstract class Polygon {
    // Attributes
    int numSides;
    String type;

    public Polygon(int numSides, String type){
        this.numSides = numSides;
        this.type = type;
    }

    public abstract float area();

    public String showData(){
        return "Type: " + type + "\nSides: " +
                numSides + "\nArea: " + area() +
                "cm^2\n";
    }
}
