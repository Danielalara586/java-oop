public class Triangle extends Polygon{
    float side01, side02, side03;
    public Triangle(float side01, float side02, float side03) {
        super(3, "triangle");
        this.side01 = side01;
        this.side02 = side02;
        this.side03 = side03;
    }

    @Override
    public float area() {
        float p = (side01 + side02 + side03)/2;
        return (float)(Math.sqrt(p * (p-side01)*(p-side02)*(p-side03)));
    }

    @Override
    public String showData() {
        return super.showData() + "Side 1: " + side01 +
                "cm. \nSide 2: " + side02 + "cm. \nSide 3: " +
                side03 + "cm. \n";
    }
}
