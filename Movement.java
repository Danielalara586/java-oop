// 2. Program that shows the coordinates of an object based on the direction
// given (UP, DOWN, LEFT, RIGHT)
public class Movement {
    // Attributes
    private String direction;
    private int x, y;

    // Constructor
    public Movement(){
        this.direction = "";
        this.x = 0;
        this.y = 0;
    }

    // Getters
    public String getDirection() {
        return direction;
    }

    // Setters
    public void setDirection(String direction) {
        this.direction = direction;
        switch (this.direction){
            case "UP":
                this.y++;
                break;
            case "DOWN":
                this.y--;
                break;
            case "RIGHT":
                this.x++;
                break;
            case "LEFT":
                this.x--;
                break;
            default:
                System.out.println("Please enter a valid option");
                break;
        }

        System.out.printf("(x: %d, y: %d)\n", this.x, this.y);
    }

		// Main
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Movement move = new Movement();
        int n;

        System.out.print("How many directions would you like to give?: ");
        n = input.nextInt();

        for (int i = 0; i < n; i++) {
            System.out.print("Enter the direction [UP, DOWN, LEFT, RIGHT]: ");
            move.setDirection(input.next().toUpperCase());
        }

    }

}
