//3. Program that given a number of cars with their attributes, shows
// all the attributes of the cheapeast car
import java.util.Scanner;

public class Car {

    // Attributes
    private String brand, color;
    private float price;

    // Constructor
    public Car(String brand, String color, float price){
        this.brand = brand;
        this.color = color;
        this.price = price;
    }

    // Getters and Setters
    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    // Method
    public void showAttributes(){
        System.out.printf("Brand: %s \nColor: %s \nPrice: $%.2f",
                getBrand(), getColor(), getPrice());
    }

    // Main
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String brand, color;
        float price, cheaper;
        int elements, position = 0;

        System.out.print("How many cars would you like to compare?: ");
        elements = input.nextInt();
        Car[] cars = new Car[elements];

        for (int i = 0; i < elements; i++) {
            System.out.print("Please enter the brand: ");
            brand = input.next();
            System.out.print("Please enter the color: ");
            color = input.next();
            System.out.print("Please enter the price: ");
            price = input.nextFloat();
            cars[i] = new Car(brand, color, price);
        }

        cheaper = cars[0].getPrice();

        for (int i = 0; i < cars.length; i++) {
            if (cars[i].price < cheaper){
                cheaper = cars[i].getPrice();
                position = i;
            }
        }

        System.out.println("Cheapest option");
        cars[position].showAttributes();

    }
}
