//6. Program that simulates a complex number calculator
import java.util.Scanner;

public class Complex {

    // Attributes
    private int real, imaginary;

    public Complex(int real, int imaginary){
        this.real = real;
        this.imaginary = imaginary;
    }

    public int getReal() {
        return real;
    }

    public int getImaginary() {
        return imaginary;
    }

    public static String addition(Complex num1, Complex num2){
        return (num1.real + num2.real) + " + " + (num1.imaginary + num2.imaginary) + "j";
    }

    public static String multiplication(Complex num1, Complex num2){
        return ((num1.real * num2.real) - (num1.imaginary * num2.imaginary)) +
                " + " + ((num1.real * num2.imaginary) + (num2.real * num1.imaginary))
                + "j";
    }

    public static String multiplyByReal(int num1, Complex num2){
        return (num1 * num2.real) + " + " + (num1 * num2.imaginary) + "j";
    }

    public static boolean isEqualTo(Complex num1, Complex num2){
        return num1.real == num2.real && num1.imaginary == num2.imaginary;
    }

    // Main
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Complex num2;
        int option, num1, real, imaginary;
        Complex[] complex = new Complex[2];

        System.out.println("Welcome to complex calculator");
        do {
            System.out.print("Which operation would you like to make?\n" +
                    "1. Add two complex numbers\n" +
                    "2. Multiply two complex numbers\n" +
                    "3. Compare two complex numbers\n" +
                    "4. Multiply an integer and a complex\n" +
                    "5. Exit\n" +
                    "Option: ");
            option = input.nextInt();

            if (option != 5){
                if (option == 4){
                    System.out.print("Please enter an integer: ");
                    num1 = input.nextInt();
                    System.out.print("Now the complex number.\nEnter the real part: ");
                    real = input.nextInt();
                    System.out.print("Enter the imaginary part: ");
                    imaginary = input.nextInt();
                    num2 = new Complex(real, imaginary);

                    System.out.println("The multiplication result is: " +
                            Complex.multiplyByReal(num1, num2) + "\n");
                }
                else{
                    for (int i = 0; i < 2; i++) {
                        System.out.printf("Add complex number %d.\nEnter the real part: ", (i+1));
                        real = input.nextInt();
                        System.out.print("Enter the imaginary part: ");
                        imaginary = input.nextInt();
                        complex[i] = new Complex(real, imaginary);
                    }

                    switch (option){
                        case 1:
                            System.out.println("Addition result: " +
                                    Complex.addition(complex[0], complex[1]) + "\n");
                            break;
                        case 2:
                            System.out.println("Multiplication result: " +
                                    Complex.multiplication(complex[0], complex[1]) + "\n");
                            break;
                        case 3:
                            System.out.println("Both numbers are equal? : " +
                                    Complex.isEqualTo(complex[0], complex[1]) + "\n");
                            break;
                        default:
                            System.out.println("Please enter a valid option\n");
                    }

                }
            }

        }while (option != 5);

        System.out.println("See you soon!");

    }
}
