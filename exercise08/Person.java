//8. Program that simulates a football team

public class Person {
    // Attributes
    String name, lastName, job;
    int age;

    public Person(String name, String lastName, int age, String job) {
        this.name = name;
        this.lastName = lastName;
        this.age = age;
        this.job = job;
    }

    public static void travel(Person[] people, String country){
        System.out.println("\nList of people traveling");
        for (int i = 0; i < people.length; i++) {
            System.out.println(people[i].showData());
        }
        System.out.println("\nTravelling to " + country + " tomorrow.");
    }

    public static void interview(Person person){
        System.out.println("\nPerson to interview.\n" + person.showData()
        + "The interview starts at 6:30pm.");

    }

    public String showData(){
        return "Complete name: " + name + " " + lastName + ". \n" +
                "Age: " + age + ". \nJob: " + job + ".\n";
    }
}
