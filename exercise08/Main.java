import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int option, age, years, dorsal;
        char peopleToAdd;
        String name, lastName, strategy, title, position;
        FootballPlayer[] team01 = new FootballPlayer[11];
        FootballPlayer[] team02 = new FootballPlayer[11];
        Trainer trainer;
        Person person = null;

        System.out.print("Which operation would you like to make?\n" +
                    "1. Travel with team\n" +
                    "2. Training\n" +
                    "3. Football match\n" +
                    "4. Training planning\n" +
                    "5. Interview\n" +
                    "6. Cure injury\n" +
                    "7. Exit\n" +
                    "Option: ");
        option = input.nextInt();

            switch (option){
                case 1:
                    int numPeople;
                    System.out.print("How many people are traveling?: ");
                    numPeople = input.nextInt();
                    Person[] people = new Person[numPeople];

                    for (int i = 0; i < numPeople; i++) {
                        System.out.print("Add [P]layer, [T]rainer or [D]octor: ");
                        peopleToAdd = input.next().charAt(0);

                        System.out.print("Please enter the name: ");
                        name = input.next();
                        System.out.print("Please enter the last name: ");
                        lastName = input.next();
                        System.out.print("Please enter the age: ");
                        age = input.nextInt();

                        switch (Character.toUpperCase(peopleToAdd)){
                            case 'P':
                                System.out.print("Please enter the dorsal: ");
                                dorsal = input.nextInt();
                                System.out.print("Please enter the position: ");
                                position = input.next();
                                people[i] = new FootballPlayer(name, lastName, age, position, dorsal);
                                break;
                            case 'T':
                                System.out.print("Please enter the strategy: ");
                                strategy = input.next();
                                people[i] = new Trainer(name, lastName, age, strategy);
                                break;
                            case 'D':
                                System.out.print("Please enter the title: ");
                                title = input.next();
                                System.out.print("Please enter the years of experience: ");
                                years = input.nextInt();
                                people[i] = new Doctor(name, lastName, age, title, years);
                                break;
                        }
                    }

                    System.out.print("Where are you travelling to?: ");
                    String country = input.next();

                    Person.travel(people, country);
                    break;

                case 2:
                    System.out.println("Trainer's data");
                    System.out.print("Please enter the name: ");
                    name = input.next();
                    System.out.print("Please enter the last name: ");
                    lastName = input.next();
                    System.out.print("Please enter the age: ");
                    age = input.nextInt();
                    System.out.print("Please enter the strategy: ");
                    strategy = input.next();
                    trainer = new Trainer(name, lastName, age, strategy);

                    System.out.println("Trainer's data");
                    for (int i = 0; i < team01.length; i++) {
                        System.out.println("Player " + (i+1));
                        System.out.print("Please enter the name: ");
                        name = input.next();
                        System.out.print("Please enter the last name: ");
                        lastName = input.next();
                        System.out.print("Please enter the age: ");
                        age = input.nextInt();
                        System.out.print("Please enter the dorsal: ");
                        dorsal = input.nextInt();
                        System.out.print("Please enter the position: ");
                        position = input.next();
                        team01[i] = new FootballPlayer(name, lastName, age, position, dorsal);
                    }

                    Trainer.training(team01, trainer);
                    break;

                case 3:
                    System.out.print("Please enter team 1 name: ");
                    String name01 = input.next();
                    System.out.println("\nFill up players");
                    for (int i = 0; i < team01.length; i++) {
                        System.out.println("Player " + (i+1));
                        System.out.print("Please enter the name: ");
                        name = input.next();
                        System.out.print("Please enter the last name: ");
                        lastName = input.next();
                        System.out.print("Please enter the age: ");
                        age = input.nextInt();
                        System.out.print("Please enter the dorsal: ");
                        dorsal = input.nextInt();
                        System.out.print("Please enter the position: ");
                        position = input.next();
                        team01[i] = new FootballPlayer(name, lastName, age, position, dorsal);
                    }

                    System.out.print("\nPlease enter team 2 name: ");
                    String name02 = input.next();
                    System.out.println("\nFill up players");

                    for (int i = 0; i < team02.length; i++) {
                        System.out.println("Player " + (i+1));
                        System.out.print("Please enter the name: ");
                        name = input.next();
                        System.out.print("Please enter the last name: ");
                        lastName = input.next();
                        System.out.print("Please enter the age: ");
                        age = input.nextInt();
                        System.out.print("Please enter the dorsal: ");
                        dorsal = input.nextInt();
                        System.out.print("Please enter the position: ");
                        position = input.next();
                        team02[i] = new FootballPlayer(name, lastName, age, position, dorsal);
                    }

                    FootballPlayer.footballMatch(team01, team02, name01, name02);
                    break;

                case 4:
                    System.out.print("Please enter the name: ");
                    name = input.next();
                    System.out.print("Please enter the last name: ");
                    lastName = input.next();
                    System.out.print("Please enter the age: ");
                    age = input.nextInt();
                    System.out.print("Please enter the strategy: ");
                    strategy = input.next();
                    trainer = new Trainer(name, lastName, age, strategy);
                    Trainer.planTraining(trainer);
                    break;

                case 5:
                    System.out.print("Who are you interviewing? [P]layer, [T]rainer or [D]octor: ");
                    peopleToAdd = input.next().charAt(0);

                    System.out.print("Please enter the name: ");
                    name = input.next();
                    System.out.print("Please enter the last name: ");
                    lastName = input.next();
                    System.out.print("Please enter the age: ");
                    age = input.nextInt();

                    switch (Character.toUpperCase(peopleToAdd)){
                        case 'P':
                            System.out.print("Please enter the dorsal: ");
                            dorsal = input.nextInt();
                            System.out.print("Please enter the position: ");
                            position = input.next();
                            person = new FootballPlayer(name, lastName, age, position, dorsal);
                            break;
                        case 'T':
                            System.out.print("Please enter the strategy: ");
                            strategy = input.next();
                            person = new Trainer(name, lastName, age, strategy);
                            break;
                        case 'D':
                            System.out.print("Please enter the title: ");
                            title = input.next();
                            System.out.print("Please enter the years of experience: ");
                            years = input.nextInt();
                            person = new Doctor(name, lastName, age, title, years);
                            break;
                    }

                    Person.interview(person);
                    break;

                case 6:
                    System.out.println("Fill doctor's data");
                    System.out.print("Please enter the name: ");
                    name = input.next();
                    System.out.print("Please enter the last name: ");
                    lastName = input.next();
                    System.out.print("Please enter the age: ");
                    age = input.nextInt();
                    System.out.print("Please enter the title: ");
                    title = input.next();
                    System.out.print("Please enter the years of experience: ");
                    years = input.nextInt();
                    Doctor doctor = new Doctor(name, lastName, age, title, years);

                    System.out.println("\nFill player's data");
                    System.out.print("Please enter the name: ");
                    name = input.next();
                    System.out.print("Please enter the last name: ");
                    lastName = input.next();
                    System.out.print("Please enter the age: ");
                    age = input.nextInt();
                    System.out.print("Please enter the dorsal: ");
                    dorsal = input.nextInt();
                    System.out.print("Please enter the position: ");
                    position = input.next();
                    FootballPlayer player = new FootballPlayer(name, lastName, age, position, dorsal);

                    System.out.print("\nEnter injury: ");
                    String injury = input.next();

                    Doctor.cureInjury(doctor, player, injury);
                    break;

                case 7:
                    System.out.println("See you soon!");
                    break;
                default:
                    System.out.println("Please enter a valid option.");
            }
    }

}
