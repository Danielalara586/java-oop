public class FootballPlayer extends Person {
    int dorsal;
    String position;

    public FootballPlayer(String name, String lastName, int age, String position, int dorsal) {
        super(name, lastName, age, "football player");
        this.dorsal = dorsal;
        this.position = position;
    }

    @Override
    public String showData() {
        return super.showData() + "Dorsal: " + dorsal +
                ".\nPosition: " + position + ".\n";
    }

    public static void footballMatch(FootballPlayer[] team01, FootballPlayer[] team02,
                                     String name01, String name02){

        System.out.println("Team " + name01 + " vs. Team " + name02 +
                "\nTeam " + name01 + " members.");
        for (int i = 0; i < team01.length; i++) {
            System.out.println(team01[i].showData());
        }
        System.out.println("\nTeam " + name02 + " members.");
        for (int i = 0; i < team02.length; i++) {
            System.out.println(team02[i].showData());
        }
        System.out.println("The match has started");
    }

}
