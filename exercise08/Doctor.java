public class Doctor extends Person{
    String title;
    int yearsOfExperience;

    public Doctor(String name, String lastName, int age, String title, int yearsOfExperience) {
        super(name, lastName, age, "doctor");
        this.title = title;
        this.yearsOfExperience = yearsOfExperience;
    }

    @Override
    public String showData() {
        return super.showData() + "Title: " + title +
                ".\nYears of experience: " + yearsOfExperience + ".\n";
    }

    public static void cureInjury(Doctor doctor, FootballPlayer player, String injury){
        System.out.println("\nDoctor attending\n" + doctor.showData()
        + "\nInjury: " + injury + "\n\nPlayer to treat\n" +
                player.showData());
        System.out.println("\nTreatment: probably pills.");
    }
}
