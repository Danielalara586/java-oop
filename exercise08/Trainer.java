public class Trainer extends Person {
    String strategy;

    public Trainer(String name, String lastName, int age, String strategy) {
        super(name, lastName, age, "trainer");
        this.strategy = strategy;
    }

    @Override
    public String showData() {
        return super.showData() + "Strategy: "  + strategy +
                ".\n";
    }

    public static void training(FootballPlayer[] players, Trainer trainer){
        System.out.printf("\nThe training is starting...\n" +
                "Trainer: %s %s.\n" +
                "Strategy: %s.\n" +
                "\nPlayers to train: \n", trainer.name,
                trainer.lastName, trainer.strategy);

        for (int i = 0; i < players.length; i++) {
            System.out.println(players[i].showData());
        }
    }

    public static void planTraining(Trainer trainer){
        System.out.printf("\nPlanning strategy by\n" +
                "Trainer: %s %s.\n" +
                "Strategy: %s.\n", trainer.name,
                trainer.lastName, trainer.strategy);
        System.out.println("\nTraining hour: 7:00am.\n" +
                "Routine: \n- Push-ups.\n- Sit-ups.\n- Chest press.\n- Game simulation.\n");
    }

}
