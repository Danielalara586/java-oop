//5. Program that given a number of triangles with their attributes, shows
// the perimeter and area of all triangles and the greatest area.
import java.util.Scanner;

public class Triangle {

    // Attributes
    private float side01, side02;

    public Triangle(float side01, float side02){
        this.side01 = side01;
        this.side02 = side02;
    }

    // Getters and setters

    public float getSide01() {
        return side01;
    }

    public void setSide01(float side01) {
        this.side01 = side01;
    }

    public float getSide02() {
        return side02;
    }

    public void setSide02(float side02) {
        this.side02 = side02;
    }

    public float perimeter(){
        return 2 * this.side02 + this.side01;
    }

    public float area(){
        return (float)((this.side01 * Math.sqrt((side02*side02) - ((side01*side01)/4)))/2);
    }

    public static float greaterArea(Triangle[] triangles){
        float area = triangles[0].area();
        for(int i = 0; i < triangles.length; i++){
            if(triangles[i].area() > area){
                area = triangles[i].area();
            }
        }

        return area;
    }

    // Main
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        float side01, side02;
        int numTriangles;

        System.out.print("How many triangles would you like to add?: ");
        numTriangles = input.nextInt();

        Triangle[] triangles = new Triangle[numTriangles];

        for (int i = 0; i < numTriangles; i++) {
            System.out.printf("\nTriangle %d \nPlease enter the base: ", (i+1));
            side01 = input.nextFloat();
            System.out.print("Please enter the side: ");
            side02 = input.nextFloat();
            triangles[i] = new Triangle(side01, side02);

            System.out.printf("\nPerimeter: %.2f\n" +
                    "Area: %.2f\n", triangles[i].perimeter(),
                    triangles[i].area());
        }

        System.out.printf("\nGreater area: %.2f", Triangle.greaterArea(triangles));

    }
}
